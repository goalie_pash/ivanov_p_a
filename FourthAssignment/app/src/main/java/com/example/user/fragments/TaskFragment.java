package com.example.user.fragments;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.user.adapters.RecyclerAdapter;
import com.example.user.adapters.ViewPagerAdapter;
import com.example.user.database.DBHelper;
import com.example.user.model.Task;
import com.example.user.fourthassignment.R;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.MODE_PRIVATE;

public class TaskFragment extends Fragment {

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    private static TaskFragment instance;
    private Unbinder unbinder;
    private ViewPagerAdapter viewPagerAdapter;
    private static final String TAB_ALL = "All";
    private static final String TAB_FAVOURITE = "Favorite";
    private static final String TAB_INDEX = "Tab Index";
    private static final String ALL_POSITION = "AllPosition";
    private static final String FAV_POSITION = "FavPosition";
    private static final String STORAGE = "Storage";
    private static final String SHARED_STORAGE = "Shared Preferences";
    private static final String EXTERNAL_STORAGE = "External";
    private static final String INTERNAL_STORAGE = "Internal";
    private static final String SQL_STORAGE = "SQL";
    private static final String EDITABLE = "Edit";
    private static final String SHARE = "Share";
    private static final String TASK = "Task";
    private ArrayList<Task> allArrayList;
    private ArrayList<Task> favArrayList;
    private AllFragment allFragment;
    private FavoriteFragment favoriteFragment;
    private NewTask newTask;
    private String storage;
    private SharedPreferences sharedPreferences;
    private RecyclerAdapter adapter;
    private DBHelper dbHelper;
    private SQLiteDatabase database;

    public static synchronized TaskFragment getInstance() {
        if (instance == null)
            instance = new TaskFragment();
        return instance;
    }

    @SuppressLint("ValidFragment")
    private TaskFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newTask = NewTask.getInstance();
        sharedPreferences = this.getActivity().getPreferences(MODE_PRIVATE);
        storage = sharedPreferences.getString(STORAGE, "");
        dbHelper = new DBHelper(this.getActivity());
        database = dbHelper.getWritableDatabase();

        if (getArguments().getBoolean(EDITABLE)) {
            allArrayList = getArguments().getParcelableArrayList(TAB_ALL);
            favArrayList = getArguments().getParcelableArrayList(TAB_FAVOURITE);
        }
        else {
            if (allArrayList == null && favArrayList == null) {
                allArrayList = new ArrayList<>();
                favArrayList = new ArrayList<>();
            }
            switch (storage) {
                case INTERNAL_STORAGE:
                    try {
                        readFromInternalStorage();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case EXTERNAL_STORAGE:
                    try {
                        readFromExternalStorage();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case SHARED_STORAGE:
                    readFromSharedPreferences();
                    break;
                case SQL_STORAGE:
                    readFromSQL();
                    break;
                default:
                    break;
            }
        }
        if (allArrayList == null && favArrayList == null) {
            allArrayList = new ArrayList<>();
            favArrayList = new ArrayList<>();
        }

        allFragment = new AllFragment(allArrayList);
        favoriteFragment = new FavoriteFragment(favArrayList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getActivity().setTitle("Tasks");
        View view = inflater.inflate(R.layout.fragment_task, container, false);
        unbinder = ButterKnife.bind(this, view);
        viewPagerAdapter = new ViewPagerAdapter(this.getActivity().getSupportFragmentManager());
        viewPagerAdapter.addFragments(allFragment, TAB_ALL);
        viewPagerAdapter.addFragments(favoriteFragment, TAB_FAVOURITE);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        registerForContextMenu(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        switch (storage) {
            case INTERNAL_STORAGE:
                writeToInternalStorage();
                break;
            case EXTERNAL_STORAGE:
                try {
                    writeToExternalStorage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case SHARED_STORAGE:
                writeToSharedPreferences();
                break;
            case SQL_STORAGE:
                writeToSQL();
                break;
            default:
                break;
        }
        allArrayList = null;
        favArrayList = null;
        getFragmentManager().beginTransaction().remove(allFragment).commit();
        getFragmentManager().beginTransaction().remove(favoriteFragment).commit();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        MenuInflater menuInflater = this.getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.item_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected (MenuItem item) {
        Task task;
        if (tabLayout.getSelectedTabPosition() == 0) adapter = allFragment.getAdapter();
        else if (tabLayout.getSelectedTabPosition() == 1) adapter = favoriteFragment.getAdapter();
        int position = adapter.getItemSelectedPosition(item);
        Bundle bundle;
        FragmentManager fragmentManager;
        switch (item.getItemId()) {
            case R.id.edit_item:
                bundle = new Bundle();
                bundle.putInt(TAB_INDEX, tabLayout.getSelectedTabPosition());
                bundle.putParcelableArrayList(TAB_ALL, allArrayList);
                bundle.putParcelableArrayList(TAB_FAVOURITE, favArrayList);
                if (tabLayout.getSelectedTabPosition() == 0) {
                    task = allArrayList.get(position);
                    bundle.putInt(ALL_POSITION, position);
                    if (favArrayList.contains(task))
                        bundle.putInt(FAV_POSITION, favArrayList.indexOf(task));
                    else
                        bundle.putInt(FAV_POSITION, -1);
                }
                else if (tabLayout.getSelectedTabPosition() == 1) {
                    task = favArrayList.get(position);
                    bundle.putInt(ALL_POSITION, allArrayList.indexOf(task));
                    bundle.putInt(FAV_POSITION, position);
                }
                newTask.setArguments(bundle);
                fragmentManager = this.getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment, newTask).commit();
                break;
            case R.id.delete_item:
                if (tabLayout.getSelectedTabPosition() == 1) {
                    task = favArrayList.get(position);
                    favArrayList.remove(task);
                    allArrayList.remove(task);
                }
                else if (tabLayout.getSelectedTabPosition() == 0) {
                    task = allArrayList.get(position);
                    allArrayList.remove(task);
                    if (favArrayList.contains(task)) favArrayList.remove(task);
                }
                viewPagerAdapter.notifyDataSetChanged();
                viewPager.setAdapter(viewPagerAdapter);
                tabLayout.setupWithViewPager(viewPager);
                break;
            case R.id.add_to_favorite_item:
                task = allArrayList.get(position);
                if (favArrayList.contains(task)) {
                    Toast.makeText(this.getActivity().getApplicationContext(), "This is favourite task!", Toast.LENGTH_SHORT).show();
                }
                else {
                    favArrayList.add(task);
                    viewPagerAdapter.notifyDataSetChanged();
                    viewPager.setAdapter(viewPagerAdapter);
                    tabLayout.setupWithViewPager(viewPager);
                }
                break;
            case R.id.share_item:
                bundle = new Bundle();
                bundle.putInt(TAB_INDEX, tabLayout.getSelectedTabPosition());
                bundle.putBoolean(SHARE, true);
                if (tabLayout.getSelectedTabPosition() == 0) {
                    task = allArrayList.get(position);
                    bundle.putParcelable(TASK, task);
                }
                else if (tabLayout.getSelectedTabPosition() == 1) {
                    task = favArrayList.get(position);
                    bundle.putParcelable(TASK, task);
                }
                newTask.setArguments(bundle);
                fragmentManager = this.getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment, newTask).commit();
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    @OnClick(R.id.fab)
    public void onFabButtonClick() {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(TAB_ALL, allArrayList);
        bundle.putParcelableArrayList(TAB_FAVOURITE, favArrayList);
        bundle.putInt(TAB_INDEX, tabLayout.getSelectedTabPosition());
        bundle.putInt(ALL_POSITION, -1);
        bundle.putInt(FAV_POSITION, -1);
        newTask.setArguments(bundle);
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment, newTask).commit();
    }

    private void readFromInternalStorage() throws IOException {
        String fileNameAll = "internal_all.txt";
        String fileNameFav = "internal_fav.txt";
        String message1;
        FileInputStream fileInputStreamAll = getActivity().openFileInput(fileNameAll);
        InputStreamReader inputStreamReaderAll = new InputStreamReader(fileInputStreamAll);
        BufferedReader bufferedReaderAll = new BufferedReader(inputStreamReaderAll);
        while ((message1 = bufferedReaderAll.readLine()) != null) {
            String[] arr = message1.split(":");
            Task task = new Task(arr[0], arr[1]);
            allArrayList.add(task);
        }
        String message2;
        FileInputStream fileInputStreamFav = getActivity().openFileInput(fileNameFav);
        InputStreamReader inputStreamReaderFav = new InputStreamReader(fileInputStreamFav);
        BufferedReader bufferedReaderFav = new BufferedReader(inputStreamReaderFav);
        while ((message2 = bufferedReaderFav.readLine()) != null) {
            String[] arr = message2.split(":");
            Task task = new Task(arr[0], arr[1]);
            favArrayList.add(task);
        }
    }

    private void writeToInternalStorage() {
        String fileNameAll = "internal_all.txt";
        String fileNameFav = "internal_fav.txt";
        try {
            FileOutputStream fileOutputStreamAll = getActivity().openFileOutput(fileNameAll, MODE_PRIVATE);
            for (Task task : allArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamAll.write(message.getBytes());
            }
            fileOutputStreamAll.close();
            FileOutputStream fileOutputStreamFav = getActivity().openFileOutput(fileNameFav, MODE_PRIVATE);
            for (Task task : favArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamFav.write(message.getBytes());
            }
            fileOutputStreamFav.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readFromExternalStorage() throws IOException {
        File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath());
        File fileAll = new File(dir, "external_all_4.txt");
        FileInputStream fileInputStreamAll = new FileInputStream(fileAll);
        InputStreamReader inputStreamReaderAll = new InputStreamReader(fileInputStreamAll);
        BufferedReader bufferedReaderAll = new BufferedReader(inputStreamReaderAll);
        String messageAll;
        while ((messageAll = bufferedReaderAll.readLine()) != null) {
            String[] arr = messageAll.split(":");
            Task task = new Task(arr[0], arr[1]);
            allArrayList.add(task);
        }
        File fileFav = new File(dir, "external_fav_4.txt");
        FileInputStream fileInputStreamFav = new FileInputStream(fileFav);
        InputStreamReader inputStreamReaderFav = new InputStreamReader(fileInputStreamFav);
        BufferedReader bufferedReaderFav = new BufferedReader(inputStreamReaderFav);
        String messageFav;
        while ((messageFav = bufferedReaderFav.readLine()) != null) {
            String[] arr = messageFav.split(":");
            Task task = new Task(arr[0], arr[1]);
            favArrayList.add(task);
        }
    }

    private void writeToExternalStorage() throws IOException {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File root = Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath());
            File fileAll = new File(dir, "external_all_4.txt");
            FileOutputStream fileOutputStreamAll = new FileOutputStream(fileAll);
            for (Task task : allArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamAll.write(message.getBytes());
            }
            fileOutputStreamAll.close();

            File fileFav = new File(dir, "external_fav_4.txt");
            if (!fileFav.exists()) fileFav.createNewFile();
            FileOutputStream fileOutputStreamFav = new FileOutputStream(fileFav);
            for (Task task : favArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamFav.write(message.getBytes());
            }
            fileOutputStreamFav.close();
        }
    }

    private void readFromSharedPreferences() {
        sharedPreferences = getActivity().getPreferences(MODE_PRIVATE);
        for (String key : sharedPreferences.getAll().keySet()) {
            Task task = new Task(key, sharedPreferences.getString(key, ""));
            allArrayList.add(task);
        }
    }

    @SuppressLint("ApplySharedPref")
    private void writeToSharedPreferences() {
        sharedPreferences = getActivity().getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (Task task : allArrayList) {
            editor.putString(task.getTitle(), task.getDescription());
        }
        editor.apply();
    }

    private void readFromSQL() {
        Cursor cursorAll = database.query(DBHelper.TABLE_STORAGE_ALL, null, null, null, null, null, null);
        if (cursorAll.moveToFirst()) {
            int titleIndex = cursorAll.getColumnIndex(DBHelper.KEY_TITLE);
            int descriptionIndex = cursorAll.getColumnIndex(DBHelper.KEY_DESCRIPTION);
            do {
                Task task = new Task(cursorAll.getString(titleIndex), cursorAll.getString(descriptionIndex));
                allArrayList.add(task);
            } while (cursorAll.moveToNext());
        }
        cursorAll.close();
        Cursor cursorFav = database.query(DBHelper.TABLE_STORAGE_FAV, null, null, null, null, null, null);
        if (cursorFav.moveToFirst()) {
            int titleIndex = cursorFav.getColumnIndex(DBHelper.KEY_TITLE);
            int descriptionIndex = cursorFav.getColumnIndex(DBHelper.KEY_DESCRIPTION);
            do {
                Task task = new Task(cursorFav.getString(titleIndex), cursorFav.getString(descriptionIndex));
                favArrayList.add(task);
            } while (cursorFav.moveToNext());
        }
        cursorFav.close();
    }

    private void writeToSQL() {
        ContentValues contentValues = new ContentValues();
        database.delete(DBHelper.TABLE_STORAGE_ALL, null, null);
        database.delete(DBHelper.TABLE_STORAGE_FAV, null, null);
        for (Task task : allArrayList) {
            contentValues.put(DBHelper.KEY_TITLE, task.getTitle());
            contentValues.put(DBHelper.KEY_DESCRIPTION, task.getDescription());
            database.insert(DBHelper.TABLE_STORAGE_ALL, null, contentValues);
        }
        for (Task task : favArrayList) {
            contentValues.put(DBHelper.KEY_TITLE, task.getTitle());
            contentValues.put(DBHelper.KEY_DESCRIPTION, task.getDescription());
            database.insert(DBHelper.TABLE_STORAGE_FAV, null, contentValues);
        }
        dbHelper.close();
    }

}
