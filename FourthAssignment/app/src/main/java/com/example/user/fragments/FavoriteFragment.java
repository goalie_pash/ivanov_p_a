package com.example.user.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.user.adapters.RecyclerAdapter;
import com.example.user.model.Task;
import com.example.user.fourthassignment.R;
import java.util.ArrayList;

public class FavoriteFragment extends Fragment {

    private ArrayList<Task> arrayList = new ArrayList<>();
    private RecyclerAdapter adapter;
    private RecyclerView recyclerView;

    public FavoriteFragment() {}

    @SuppressLint("ValidFragment")
    public FavoriteFragment(ArrayList<Task> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        recyclerView = view.findViewById(R.id.recycler_fav);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        adapter = new RecyclerAdapter(arrayList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        registerForContextMenu(recyclerView);
        registerForContextMenu(view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public RecyclerAdapter getAdapter() {
        return adapter;
    }

}
