package com.example.user.interfaces;

public interface LongClickListener {
    void onItemLongClick(int position);
}
