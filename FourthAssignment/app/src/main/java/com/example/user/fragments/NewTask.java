package com.example.user.fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.model.Task;
import com.example.user.fourthassignment.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NewTask extends Fragment {

    @BindView(R.id.field_title)
    EditText titleEditText;

    @BindView(R.id.field_description)
    EditText descriptionEditText;

    @BindView(R.id.save_button)
    Button saveButton;

    private static NewTask instance;
    private Unbinder unbinder;
    private static final String TAB_ALL = "All";
    private static final String TAB_FAVOURITE = "Favorite";
    private static final String TAB_INDEX = "Tab Index";
    private static final String ALL_POSITION = "AllPosition";
    private static final String FAV_POSITION = "FavPosition";
    private static final String EDITABLE = "Edit";
    private static final String SHARE = "Share";
    private static final String TASK = "Task";
    private int tabIndex;
    private ArrayList<Task> allArrayList;
    private ArrayList<Task> favArrayList;
    private int allPosition;
    private int favPosition;
    private boolean share;
    private Task task;

    public static synchronized NewTask getInstance() {
        if (instance == null)
            instance = new NewTask();
        return instance;
    }

    @SuppressLint("ValidFragment")
    private NewTask() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_task, container, false);
        unbinder = ButterKnife.bind(this, view);

        getActivity().setTitle("New Task");

        Bundle bundle = getArguments();
        share = bundle.getBoolean(SHARE, false);

        if (share) {
            task = bundle.getParcelable(TASK);
        }
        else {
            allArrayList = bundle.getParcelableArrayList(TAB_ALL);
            favArrayList = bundle.getParcelableArrayList(TAB_FAVOURITE);
            tabIndex = bundle.getInt(TAB_INDEX);
            allPosition = bundle.getInt(ALL_POSITION);
            favPosition = bundle.getInt(FAV_POSITION);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (share) {
            titleEditText.setText(task.getTitle());
            descriptionEditText.setText(task.getDescription());
            Toast.makeText(this.getActivity().getApplicationContext(), "Screen captured!", Toast.LENGTH_SHORT).show();
            titleEditText.setEnabled(false);
            descriptionEditText.setEnabled(false);
            titleEditText.setTextColor(Color.BLACK);
            descriptionEditText.setTextColor(Color.BLACK);
        }
        else {
            if (allPosition != -1) {
                if (tabIndex == 0) {
                    Task task = allArrayList.get(allPosition);
                    titleEditText.setText(task.getTitle());
                    descriptionEditText.setText(task.getDescription());
                } else if (tabIndex == 1) {
                    Task task = favArrayList.get(favPosition);
                    titleEditText.setText(task.getTitle());
                    descriptionEditText.setText(task.getDescription());
                }
            } else {
                titleEditText.setText("");
                descriptionEditText.setText("");
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        titleEditText.setText("");
        descriptionEditText.setText("");
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.save_button)
    public void onSaveButtonClick() {
        String title = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();

        Bundle bundle = new Bundle();

        if (!share) {
            if (allPosition != -1) {
                if (tabIndex == 0) {
                    if (favPosition != -1) {
                        favArrayList.get(favPosition).setTitle(title);
                        favArrayList.get(favPosition).setDescription(description);
                    }
                    allArrayList.get(allPosition).setTitle(title);
                    allArrayList.get(allPosition).setDescription(description);
                }
                else if (tabIndex == 1) {
                    allArrayList.get(allPosition).setTitle(title);
                    allArrayList.get(allPosition).setDescription(description);
                    favArrayList.get(favPosition).setTitle(title);
                    favArrayList.get(favPosition).setDescription(description);
                }
            }
            else {
                Task task = new Task(title, description);
                allArrayList.add(task);
                if (tabIndex == 1) favArrayList.add(task);
            }
            bundle.putParcelableArrayList(TAB_ALL, allArrayList);
            bundle.putParcelableArrayList(TAB_FAVOURITE, favArrayList);
            bundle.putBoolean(EDITABLE, true);
        }
        else {
            takeScreenshot();
        }

        TaskFragment taskFragment = TaskFragment.getInstance();
        taskFragment.setArguments(bundle);
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment, taskFragment).commit();
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";
            View v1 = getActivity().getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            File imageFile = new File(mPath);
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            openScreenshot(imageFile);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

}
