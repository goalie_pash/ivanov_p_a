package com.example.user.firstassignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CheckBox floatCheckBox;
    private CheckBox signedCheckBox;
    private EditText editText1;
    private EditText editText2;
    private RadioGroup radioGroup;
    private Button resultButton;
    private TextView resultTextView;
    private String operation = "";
    private static final String TAG = "MainActivity";
    private static final String KEY_RESULT = "result";
    private String result = "";
    final int MENU_RESET_ID = 1;
    final int MENU_QUIT_ID = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultTextView = findViewById(R.id.textResult);

        if (savedInstanceState != null) {
            result = savedInstanceState.getString(KEY_RESULT);
            resultTextView.setText(result);
        }

        editText1 = findViewById(R.id.field1);
        editText2 = findViewById(R.id.field2);

        floatCheckBox = findViewById(R.id.floatValues);
        signedCheckBox = findViewById(R.id.signedValues);

        floatCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !signedCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                }
                else if (isChecked && signedCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                }
                else if (!isChecked && !signedCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
                else if (!isChecked && signedCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                }
            }
        });

        signedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !floatCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                }
                else if (isChecked && floatCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                }
                else if (!isChecked && !floatCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
                else if (!isChecked && floatCheckBox.isChecked()) {
                    editText1.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    editText2.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                }
            }
        });

        radioGroup = findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioPlus:
                        operation = "plus";
                        break;
                    case R.id.radioMinus:
                        operation = "minus";
                        break;
                    case R.id.radioMultiple:
                        operation = "multiple";
                        break;
                    case R.id.radioDivision:
                        operation = "division";
                        break;
                    default:
                        break;
                }
            }
        });


        resultButton = findViewById(R.id.calculateButton);
        resultButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                double a1, a2;
                //String result;
                if (editText1.getText().toString().equals(""))
                    Toast.makeText(getApplicationContext(), "Filed 1 is not filled!", Toast.LENGTH_SHORT).show();
                else if (editText2.getText().toString().equals(""))
                    Toast.makeText(getApplicationContext(), "Filed 2 is not filled!", Toast.LENGTH_SHORT).show();
                switch (operation) {
                    case "plus":
                        a1 = Double.parseDouble(editText1.getText().toString());
                        a2 = Double.parseDouble(editText2.getText().toString());
                        result = String.valueOf(a1 + a2);
                        resultTextView.setText(result);
                        break;
                    case "minus":
                        a1 = Double.parseDouble(editText1.getText().toString());
                        a2 = Double.parseDouble(editText2.getText().toString());
                        result = String.valueOf(a1 - a2);
                        resultTextView.setText(result);
                        break;
                    case "multiple":
                        a1 = Double.parseDouble(editText1.getText().toString());
                        a2 = Double.parseDouble(editText2.getText().toString());
                        result = String.valueOf(a1 * a2);
                        resultTextView.setText(result);
                        break;
                    case "division":
                        a1 = Double.parseDouble(editText1.getText().toString());
                        a2 = Double.parseDouble(editText2.getText().toString());
                        if (a2 == 0) {
                            Toast.makeText(getApplicationContext(), "Argument 'divisor' is 0!", Toast.LENGTH_SHORT).show();
                            break;
                        }
                        result = String.valueOf(a1 / a2);
                        resultTextView.setText(result);
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Operaion not found message!", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putString(KEY_RESULT, result);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_RESET_ID, 0, "Reset");
        menu.add(0, MENU_QUIT_ID, 0, "Quit");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_RESET_ID:
                editText1.setText("");
                editText2.setText("");
                resultTextView.setText("");
                radioGroup.clearCheck();
                break;
            case MENU_QUIT_ID:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
