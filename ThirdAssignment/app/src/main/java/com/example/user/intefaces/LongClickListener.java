package com.example.user.intefaces;

public interface LongClickListener {
    void onItemLongClick(int position);
}
