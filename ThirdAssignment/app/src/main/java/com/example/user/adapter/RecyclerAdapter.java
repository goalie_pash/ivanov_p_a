package com.example.user.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.user.intefaces.LongClickListener;
import com.example.user.model.Task;
import com.example.user.thirdassignment.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private ArrayList<Task> items;
    private int pos;

    public RecyclerAdapter(ArrayList<Task> items) {
        this.items = items;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bind(items.get(position));

        holder.setLongClickListener(new LongClickListener() {
            @Override
            public void onItemLongClick(int position) {
                pos = position;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getItemSelectedPosition(MenuItem menuItem) {
        return pos;
    }
}
