package com.example.user.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.example.user.thirdassignment.MainActivity;
import com.example.user.thirdassignment.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;

public class Settings extends Fragment {

    @BindView(R.id.settings_radio_group)
    RadioGroup settingsRadioGroup;

    private Unbinder unbinder;
    private static Settings instance;
    private String storage;
    private static final String STORAGE = "Storage";
    private static final String SHARED_STORAGE = "Shared Preferences";
    private static final String EXTERNAL_STORAGE = "External";
    private static final String INTERNAL_STORAGE = "Internal";
    private static final String SQL_STORAGE = "SQL";
    private SharedPreferences sharedPreferences;

    public static synchronized Settings getInstance() {
        if (instance == null)
            instance = new Settings();
        return instance;
    }

    @SuppressLint("ValidFragment")
    private Settings() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);

        settingsRadioGroup = view.findViewById(R.id.settings_radio_group);
        sharedPreferences = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        storage = sharedPreferences.getString(STORAGE, "");
        switch (storage) {
            case INTERNAL_STORAGE:
                settingsRadioGroup.check(R.id.radio_internal);
                break;
            case EXTERNAL_STORAGE:
                settingsRadioGroup.check(R.id.radio_external);
                break;
            case SHARED_STORAGE:
                settingsRadioGroup.check(R.id.radio_shared_pref);
                break;
            case SQL_STORAGE:
                settingsRadioGroup.check(R.id.radio_sql);
                break;
            default:
                break;
        }

        settingsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_shared_pref:
                        storage = SHARED_STORAGE;
                        break;
                    case R.id.radio_external:
                        storage = EXTERNAL_STORAGE;
                        break;
                    case R.id.radio_internal:
                        storage = INTERNAL_STORAGE;
                        break;
                    case R.id.radio_sql:
                        storage = SQL_STORAGE;
                        break;
                    default:
                        break;
                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(STORAGE, storage);
                editor.apply();
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
