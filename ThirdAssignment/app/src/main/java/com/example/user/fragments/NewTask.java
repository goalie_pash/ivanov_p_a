package com.example.user.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.example.user.model.Task;
import com.example.user.thirdassignment.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewTask extends Fragment {

    @BindView(R.id.field_title)
    EditText titleEditText;

    @BindView(R.id.field_description)
    EditText descriptionEditText;

    @BindView(R.id.save_button)
    Button saveButton;

    private static NewTask instance;
    private Unbinder unbinder;
    private static final String TAB_ALL = "All";
    private static final String TAB_FAVOURITE = "Favorite";
    private static final String TAB_INDEX = "Tab Index";
    private static final String ALL_POSITION = "AllPosition";
    private static final String FAV_POSITION = "FavPosition";
    private static final String EDITABLE = "Edit";
    private int tabIndex;
    private ArrayList<Task> allArrayList;
    private ArrayList<Task> favArrayList;
    private int allPosition;
    private int favPosition;


    public static synchronized NewTask getInstance() {
        if (instance == null)
            instance = new NewTask();
        return instance;
    }

    @SuppressLint("ValidFragment")
    private NewTask() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_task, container, false);
        unbinder = ButterKnife.bind(this, view);

        getActivity().setTitle("New Task");

        Bundle bundle = getArguments();
        allArrayList = bundle.getParcelableArrayList(TAB_ALL);
        favArrayList = bundle.getParcelableArrayList(TAB_FAVOURITE);
        tabIndex = bundle.getInt(TAB_INDEX);
        allPosition = bundle.getInt(ALL_POSITION);
        favPosition = bundle.getInt(FAV_POSITION);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (allPosition != -1) {
            if (tabIndex == 0) {
                Task task = allArrayList.get(allPosition);
                titleEditText.setText(task.getTitle());
                descriptionEditText.setText(task.getDescription());
            }
            else if (tabIndex == 1) {
                Task task = favArrayList.get(favPosition);
                titleEditText.setText(task.getTitle());
                descriptionEditText.setText(task.getDescription());
            }
        }
        else {
            titleEditText.setText("");
            descriptionEditText.setText("");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        titleEditText.setText("");
        descriptionEditText.setText("");
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.save_button)
    public void onSaveButtonClick() {
        String title = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();

        if (allPosition != -1) {
            if (tabIndex == 0) {
                if (favPosition != -1) {
                    favArrayList.get(favPosition).setTitle(title);
                    favArrayList.get(favPosition).setDescription(description);
                }
                allArrayList.get(allPosition).setTitle(title);
                allArrayList.get(allPosition).setDescription(description);
            }
            else if (tabIndex == 1) {
                allArrayList.get(allPosition).setTitle(title);
                allArrayList.get(allPosition).setDescription(description);
                favArrayList.get(favPosition).setTitle(title);
                favArrayList.get(favPosition).setDescription(description);
            }
        }
        else {
            Task task = new Task(title, description);
            allArrayList.add(task);
            if (tabIndex == 1) favArrayList.add(task);
        }

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(TAB_ALL, allArrayList);
        bundle.putParcelableArrayList(TAB_FAVOURITE, favArrayList);
        bundle.putBoolean(EDITABLE, true);
        TaskFragment taskFragment = TaskFragment.getInstance();
        taskFragment.setArguments(bundle);
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment, taskFragment).commit();
    }

}
