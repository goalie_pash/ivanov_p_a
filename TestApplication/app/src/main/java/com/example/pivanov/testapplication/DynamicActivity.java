package com.example.pivanov.testapplication;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.view.View.OnClickListener;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class DynamicActivity extends AppCompatActivity implements OnClickListener {

    JSONArray jsonArray;
    JsonObject request;
    TableLayout mainTableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic);
        String json = getIntent().getStringExtra("json");
        TableLayout mainTableLayout = findViewById(R.id.mainTableLayout);
        try {
            JSONObject jsonObject = new JSONObject(json);
            setTitle(jsonObject.get("title").toString());
            jsonArray = jsonObject.getJSONArray("fields");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject item = (JSONObject) jsonArray.get(i);
                if (item.get("type").equals("TEXT")) {
                    EditText editText = new EditText(this);
                    mainTableLayout.addView(editText);
                    System.out.println();
                }
                else if (item.get("type").equals("NUMERIC")) {
                    EditText editText = new EditText(this);
                    editText.setInputType(3);
                    mainTableLayout.addView(editText);
                }
                else if (item.get("type").equals("LIST")) {
                    JSONObject values = (JSONObject)item.get("values");
                    JSONArray array = values.toJSONArray(values.names());
                    String[] data = new String[array.length()];
                    for (int j = 0; j < data.length; j++) {
                        data[j] = array.get(j).toString();
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner spinner = new Spinner(this);
                    spinner.setAdapter(adapter);
                    spinner.setPrompt("Список");
                    spinner.setSelection(0);
                    mainTableLayout.addView(spinner);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Button sendButton = new Button(this);
        sendButton.setText("Send");
        sendButton.setOnClickListener(this);
        mainTableLayout.addView(sendButton);

    }

    @Override
    public void onClick(View view) {
        mainTableLayout = findViewById(R.id.mainTableLayout);
        JsonObject formRequest = new JsonObject();
        request = new JsonObject();
        for (int i = 0; i < jsonArray.length(); i++) {
            if (mainTableLayout.getChildAt(i) instanceof  Spinner) {
                Spinner spinner = (Spinner) mainTableLayout.getChildAt(i);
                try {
                    JSONObject item = (JSONObject) jsonArray.get(i);
                    formRequest.addProperty((String) item.get("name"), spinner.getSelectedItem().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                EditText editText = (EditText) mainTableLayout.getChildAt(i);
                try {
                    JSONObject item = (JSONObject) jsonArray.get(i);
                    if (item.get("name").equals("numeric"))
                        formRequest.addProperty((String) item.get("name"), Double.parseDouble(editText.getText().toString()));
                    else
                        formRequest.addProperty((String) item.get("name"), editText.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        request.add("form", formRequest);
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("request", request.toString());
        startActivity(intent);
    }
}
