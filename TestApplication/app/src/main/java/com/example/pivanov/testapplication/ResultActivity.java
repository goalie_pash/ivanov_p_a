package com.example.pivanov.testapplication;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TableLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONObject;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ResultActivity extends AppCompatActivity {

    TableLayout mainTableLayout;
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json");
    private final String url = "http://test.clevertec.ru/tt/data";
    private final OkHttpClient client = new OkHttpClient();
    private String result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Результат");
        String req = getIntent().getStringExtra("request");
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(req, JsonElement.class);
        final JsonObject request = element.getAsJsonObject();


        run(request);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Результат");
        builder.setMessage(result);
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }



    public void run(JsonObject request) {
        RequestBody body = RequestBody.create(MEDIA_TYPE, request.toString());
        final Request request1 = new Request.Builder().url(url).post(body).build();
        client.newCall(request1).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage();
                Log.w("failure Response", mMessage);
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String mMessage = response.body().string();
                if (response.isSuccessful()){
                    try {
                        JSONObject json = new JSONObject(mMessage);
                        result = json.toString();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
