package com.example.user.secondassignment;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;
import com.danikula.aibolit.Aibolit;
import com.danikula.aibolit.annotation.InjectOnClickListener;
import com.danikula.aibolit.annotation.InjectView;
import com.example.user.database.DBHelper;
import com.example.user.fragment.AllFragment;
import com.example.user.adapter.ViewPagerAdapter;
import com.example.user.fragment.FavouriteFragment;
import com.example.user.model.Task;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class TaskActivity extends AppCompatActivity {

    @InjectView(R.id.viewpager)
    ViewPager viewPager;

    @InjectView(R.id.sliding_tabs)
    TabLayout tabLayout;

    @InjectView(R.id.fab)
    FloatingActionButton fab;

    @InjectView(R.id.main_button)
    Button mainButton;

    private ArrayList<Task> allArrayList;
    private ArrayList<Task> favArrayList;
    private ViewPagerAdapter viewPagerAdapter;
    private static final String TAB_ALL = "All";
    private static final String TAB_FAVOURITE = "Favourite";
    private static final String TAB_INDEX = "Tab Index";
    private static final String ALL_POSITION = "AllPosition";
    private static final String FAV_POSITION = "FavPosition";
    private static final String STORAGE = "Storage";
    private static final String SHARED_STORAGE = "Shared Preferences";
    private static final String EXTERNAL_STORAGE = "External";
    private static final String INTERNAL_STORAGE = "Internal";
    private static final String SQL_STORAGE = "SQL";
    private static final String MEMORY_STORAGE = "Memory";
    private String storage;
    private static final String EDITABLE = "Edit";
    private SharedPreferences sharedPreferences;
    private DBHelper dbHelper;
    private SQLiteDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        setTitle("Tasks");

        Aibolit.doInjections(this);

        storage = getIntent().getStringExtra(STORAGE);

        dbHelper = new DBHelper(this);
        database = dbHelper.getWritableDatabase();

        allArrayList = new ArrayList<>();
        favArrayList = new ArrayList<>();

        if (getIntent().getBooleanExtra(EDITABLE, false)) {
            allArrayList = getIntent().getParcelableArrayListExtra(TAB_ALL);
            favArrayList = getIntent().getParcelableArrayListExtra(TAB_FAVOURITE);
        }
        else {
            switch (storage) {
                case INTERNAL_STORAGE:
                    try {
                        readFromInternalStorage();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case EXTERNAL_STORAGE:
                    try {
                        readFromExternalStorage();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case SHARED_STORAGE:
                    readFromSharedPreferences();
                    break;
                case SQL_STORAGE:
                    readFromSQL();
                    break;
                default:
                    break;
            }
        }
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new AllFragment(allArrayList), TAB_ALL);
        viewPagerAdapter.addFragments(new FavouriteFragment(favArrayList), TAB_FAVOURITE);
        updateTabLayout();
    }



    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.item_context_menu, menu);
    }


    @Override
    public boolean onContextItemSelected (MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Task task;
        switch (item.getItemId()) {
            case R.id.edit_item:
                Intent intent = new Intent(TaskActivity.this, NewTaskActivity.class);
                intent.putExtra(TAB_INDEX, tabLayout.getSelectedTabPosition());
                intent.putParcelableArrayListExtra(TAB_FAVOURITE, favArrayList);
                intent.putParcelableArrayListExtra(TAB_ALL, allArrayList);
                if (tabLayout.getSelectedTabPosition() == 0) {
                    task = allArrayList.get(info.position);
                    intent.putExtra(ALL_POSITION, info.position);
                    if (favArrayList.contains(task))
                        intent.putExtra(FAV_POSITION, favArrayList.indexOf(task));
                }
                else if (tabLayout.getSelectedTabPosition() == 1) {
                    task = favArrayList.get(info.position);
                    intent.putExtra(ALL_POSITION, allArrayList.indexOf(task));
                    intent.putExtra(FAV_POSITION, info.position);
                }
                startActivity(intent);
                return true;
            case R.id.delete_item:
                if (tabLayout.getSelectedTabPosition() == 1) {
                    task = favArrayList.get(info.position);
                    favArrayList.remove(task);
                    allArrayList.remove(task);
                }
                else if (tabLayout.getSelectedTabPosition() == 0) {
                    task = allArrayList.get(info.position);
                    allArrayList.remove(task);
                    if (favArrayList.contains(task)) favArrayList.remove(task);
                }
                viewPagerAdapter.notifyDataSetChanged();
                updateTabLayout();
                return true;
            case R.id.add_to_favorite_item:
                task = allArrayList.get(info.position);
                if (favArrayList.contains(task)) {
                    Toast.makeText(getApplicationContext(), "This is favourite task!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                favArrayList.add(task);
                viewPagerAdapter.notifyDataSetChanged();
                updateTabLayout();
                return true;
            default:
                return false;
        }
    }


    @InjectOnClickListener(R.id.fab)
    private void onFabButtonClick(View view) {
        Intent intent = new Intent(TaskActivity.this, NewTaskActivity.class);
        intent.putParcelableArrayListExtra(TAB_FAVOURITE, favArrayList);
        intent.putParcelableArrayListExtra(TAB_ALL, allArrayList);
        intent.putExtra(TAB_INDEX, tabLayout.getSelectedTabPosition());
        intent.putExtra(STORAGE, storage);
        startActivity(intent);
    }


    @InjectOnClickListener(R.id.main_button)
    private void onBackToMainButton(View view) throws IOException {
        switch (storage) {
            case INTERNAL_STORAGE:
                writeToInternalStorage();
                break;
            case EXTERNAL_STORAGE:
                writeToExternalStorage();
                break;
            case SHARED_STORAGE:
                writeToSharedPreferences();
                break;
            case SQL_STORAGE:
                writeToSQL();
                break;
            default:
                break;
        }
        Intent intent = new Intent(TaskActivity.this, MainActivity.class);
        intent.putExtra(STORAGE, storage);
        startActivity(intent);
    }


    private void updateTabLayout() {
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void readFromInternalStorage() throws IOException {
        String fileNameAll = "internal_all.txt";
        String fileNameFav = "internal_fav.txt";
        String message1;
        FileInputStream fileInputStreamAll = openFileInput(fileNameAll);
        InputStreamReader inputStreamReaderAll = new InputStreamReader(fileInputStreamAll);
        BufferedReader bufferedReaderAll = new BufferedReader(inputStreamReaderAll);
        while ((message1 = bufferedReaderAll.readLine()) != null) {
            String[] arr = message1.split(":");
            Task task = new Task(arr[0], arr[1]);
            allArrayList.add(task);
        }
        String message2;
        FileInputStream fileInputStreamFav = openFileInput(fileNameFav);
        InputStreamReader inputStreamReaderFav = new InputStreamReader(fileInputStreamFav);
        BufferedReader bufferedReaderFav = new BufferedReader(inputStreamReaderFav);
        while ((message2 = bufferedReaderFav.readLine()) != null) {
            String[] arr = message2.split(":");
            Task task = new Task(arr[0], arr[1]);
            favArrayList.add(task);
        }
    }


    private void writeToInternalStorage() {
        String fileNameAll = "internal_all.txt";
        String fileNameFav = "internal_fav.txt";
        try {
            FileOutputStream fileOutputStreamAll = openFileOutput(fileNameAll, MODE_PRIVATE);
            for (Task task : allArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamAll.write(message.getBytes());
            }
            fileOutputStreamAll.close();
            FileOutputStream fileOutputStreamFav = openFileOutput(fileNameFav, MODE_PRIVATE);
            for (Task task : favArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamFav.write(message.getBytes());
            }
            fileOutputStreamFav.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void readFromExternalStorage() throws IOException {
        File root = Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath());
        File fileAll = new File(dir, "external_all.txt");
        FileInputStream fileInputStreamAll = new FileInputStream(fileAll);
        InputStreamReader inputStreamReaderAll = new InputStreamReader(fileInputStreamAll);
        BufferedReader bufferedReaderAll = new BufferedReader(inputStreamReaderAll);
        String messageAll;
        while ((messageAll = bufferedReaderAll.readLine()) != null) {
            String[] arr = messageAll.split(":");
            Task task = new Task(arr[0], arr[1]);
            allArrayList.add(task);
        }
        File fileFav = new File(dir, "external_fav.txt");
        FileInputStream fileInputStreamFav = new FileInputStream(fileFav);
        InputStreamReader inputStreamReaderFav = new InputStreamReader(fileInputStreamFav);
        BufferedReader bufferedReaderFav = new BufferedReader(inputStreamReaderFav);
        String messageFav;
        while ((messageFav = bufferedReaderFav.readLine()) != null) {
            String[] arr = messageFav.split(":");
            Task task = new Task(arr[0], arr[1]);
            favArrayList.add(task);
        }
    }


    private void writeToExternalStorage() throws IOException {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File root = Environment.getExternalStorageDirectory();
            File dir = new File(root.getAbsolutePath());
            File fileAll = new File(dir, "external_all.txt");
            FileOutputStream fileOutputStreamAll = new FileOutputStream(fileAll);
            for (Task task : allArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamAll.write(message.getBytes());
            }
            fileOutputStreamAll.close();

            File fileFav = new File(dir, "external_fav.txt");
            if (!fileFav.exists()) fileFav.createNewFile();
            FileOutputStream fileOutputStreamFav = new FileOutputStream(fileFav);
            for (Task task : favArrayList) {
                String message = task.getTitle() + ":" + task.getDescription() + "\n";
                fileOutputStreamFav.write(message.getBytes());
            }
            fileOutputStreamFav.close();
        }
    }


    private void readFromSharedPreferences() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        for (String key : sharedPreferences.getAll().keySet()) {
            Task task = new Task(key, sharedPreferences.getString(key, ""));
            allArrayList.add(task);
        }
    }


    @SuppressLint("ApplySharedPref")
    private void writeToSharedPreferences() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (Task task : allArrayList) {
            editor.putString(task.getTitle(), task.getDescription());
        }
        editor.apply();
    }


    private void readFromSQL() {
        Cursor cursorAll = database.query(DBHelper.TABLE_STORAGE_ALL, null, null, null, null, null, null);
        if (cursorAll.moveToFirst()) {
            int titleIndex = cursorAll.getColumnIndex(DBHelper.KEY_TITLE);
            int descriptionIndex = cursorAll.getColumnIndex(DBHelper.KEY_DESCRIPTION);
            do {
                Task task = new Task(cursorAll.getString(titleIndex), cursorAll.getString(descriptionIndex));
                allArrayList.add(task);
            } while (cursorAll.moveToNext());
        }
        cursorAll.close();
        Cursor cursorFav = database.query(DBHelper.TABLE_STORAGE_FAV, null, null, null, null, null, null);
        if (cursorFav.moveToFirst()) {
            int titleIndex = cursorFav.getColumnIndex(DBHelper.KEY_TITLE);
            int descriptionIndex = cursorFav.getColumnIndex(DBHelper.KEY_DESCRIPTION);
            do {
                Task task = new Task(cursorFav.getString(titleIndex), cursorFav.getString(descriptionIndex));
                favArrayList.add(task);
            } while (cursorFav.moveToNext());
        }
        cursorFav.close();
    }


    private void writeToSQL() {
        ContentValues contentValues = new ContentValues();
        database.delete(DBHelper.TABLE_STORAGE_ALL, null, null);
        database.delete(DBHelper.TABLE_STORAGE_FAV, null, null);
        for (Task task : allArrayList) {
            contentValues.put(DBHelper.KEY_TITLE, task.getTitle());
            contentValues.put(DBHelper.KEY_DESCRIPTION, task.getDescription());
            database.insert(DBHelper.TABLE_STORAGE_ALL, null, contentValues);
        }
        for (Task task : favArrayList) {
            contentValues.put(DBHelper.KEY_TITLE, task.getTitle());
            contentValues.put(DBHelper.KEY_DESCRIPTION, task.getDescription());
            database.insert(DBHelper.TABLE_STORAGE_FAV, null, contentValues);
        }
        dbHelper.close();
    }
}
