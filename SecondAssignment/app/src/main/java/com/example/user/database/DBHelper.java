package com.example.user.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    // public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "storageDB";
    public static final String TABLE_STORAGE_ALL = "storageAll";
    public static final String TABLE_STORAGE_FAV = "storageFav";

    public static final String KEY_ID = "_id";
    public static final String KEY_TITLE = "_title";
    public static final String KEY_DESCRIPTION = "_description";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_STORAGE_ALL + "(" + KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_TITLE + " TEXT," + KEY_DESCRIPTION + " TEXT" + ")");
        db.execSQL("CREATE TABLE " + TABLE_STORAGE_FAV + "(" + KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_TITLE + " TEXT," + KEY_DESCRIPTION + " TEXT" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORAGE_ALL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORAGE_FAV);
        onCreate(db);
    }

}
