package com.example.user.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.user.adapter.ItemAdapter;
import com.example.user.model.Task;
import com.example.user.secondassignment.R;
import java.util.ArrayList;

public class FavouriteFragment extends Fragment {

    private ArrayList<Task> arrayList = new ArrayList<>();

    public FavouriteFragment() {}

    @SuppressLint("ValidFragment")
    public FavouriteFragment(ArrayList<Task> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        ListView listView = view.findViewById(R.id.favListView);
        ItemAdapter adapter = new ItemAdapter(getActivity(), R.layout.list_item, arrayList);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        return view;
    }

}
