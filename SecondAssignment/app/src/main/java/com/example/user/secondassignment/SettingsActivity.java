package com.example.user.secondassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.danikula.aibolit.Aibolit;
import com.danikula.aibolit.annotation.InjectOnClickListener;
import com.danikula.aibolit.annotation.InjectView;

public class SettingsActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private Button backToMainButton;
    private String storage;
    private static final String STORAGE = "Storage";
    private static final String SHARED_STORAGE = "Shared Preferences";
    private static final String EXTERNAL_STORAGE = "External";
    private static final String INTERNAL_STORAGE = "Internal";
    private static final String SQL_STORAGE = "SQL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle("Settings");

        radioGroup = findViewById(R.id.settingsRadioGroup);
        backToMainButton = findViewById(R.id.back_to_main);

        storage = getIntent().getStringExtra(STORAGE);
        switch (storage) {
            case SHARED_STORAGE:
                radioGroup.check(R.id.radioSharedPref);
                break;
            case EXTERNAL_STORAGE:
                radioGroup.check(R.id.radioExternal);
                break;
            case INTERNAL_STORAGE:
                radioGroup.check(R.id.radioInternal);
                break;
            case SQL_STORAGE:
                radioGroup.check(R.id.radioSQL);
                break;
            default:
                break;
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioSharedPref:
                        storage = SHARED_STORAGE;
                        break;
                    case R.id.radioExternal:
                        storage = EXTERNAL_STORAGE;
                        break;
                    case R.id.radioInternal:
                        storage = INTERNAL_STORAGE;
                        break;
                    case R.id.radioSQL:
                        storage = SQL_STORAGE;
                        break;
                    default:
                        break;
                }
            }
        });

        backToMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                intent.putExtra(STORAGE, storage);
                startActivity(intent);
            }
        });
    }
}
