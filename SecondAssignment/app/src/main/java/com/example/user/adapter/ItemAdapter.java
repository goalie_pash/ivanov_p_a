package com.example.user.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.user.model.Task;
import com.example.user.secondassignment.R;

import java.util.ArrayList;

public class ItemAdapter extends ArrayAdapter<Task> {

    private LayoutInflater mInflater;
    private int mViewResourceId;
    private ArrayList<Task> arrayList;

    public ItemAdapter(Context ctx, int viewResourceId, ArrayList<Task> arrayList) {
        super(ctx, viewResourceId, arrayList);
        mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mViewResourceId = viewResourceId;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Task getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        TextView titleTextView = convertView.findViewById(R.id.item_title);
        titleTextView.setText(arrayList.get(position).getTitle());

        TextView descriptionTextView = convertView.findViewById(R.id.item_description);
        descriptionTextView.setText(arrayList.get(position).getDescription());

        return convertView;
    }
}
