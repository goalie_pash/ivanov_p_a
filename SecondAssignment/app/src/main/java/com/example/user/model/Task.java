package com.example.user.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class Task implements Parcelable {

    private String title;
    private String description;

    private Task(Parcel parcel) {
        title = parcel.readString();
        description = parcel.readString();
    }

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Task)) return false;
        Task other = (Task)obj;
        return Objects.equals(title, other.title) && Objects.equals(description, other.description);
    }
}
