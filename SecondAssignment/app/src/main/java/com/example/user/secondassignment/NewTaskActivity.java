package com.example.user.secondassignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.danikula.aibolit.Aibolit;
import com.danikula.aibolit.annotation.InjectOnClickListener;
import com.danikula.aibolit.annotation.InjectView;
import com.example.user.model.Task;

import java.util.ArrayList;

public class NewTaskActivity extends AppCompatActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.save_button)
    Button saveButton;

    @InjectView(R.id.fieldTitle)
    EditText fieldTitle;

    @InjectView(R.id.fieldDescription)
    EditText fieldDescription;

    private ArrayList<Task> allArrayList;
    private ArrayList<Task> favArrayList;
    private int tabIndex;
    private int allPosition;
    private int favPosition;
    private static final String TAB_ALL = "All";
    private static final String TAB_FAVOURITE = "Favourite";
    private static final String TAB_INDEX = "Tab Index";
    private static final String ALL_POSITION = "AllPosition";
    private static final String FAV_POSITION = "FavPosition";
    private String storage;
    private static final String STORAGE = "Storage";
    private static final String EDITABLE = "Edit";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        Aibolit.doInjections(this);
        setSupportActionBar(toolbar);

        storage = getIntent().getStringExtra(STORAGE);

        tabIndex = getIntent().getIntExtra(TAB_INDEX, -1);
        allPosition = getIntent().getIntExtra(ALL_POSITION, -1);
        favPosition = getIntent().getIntExtra(FAV_POSITION, -1);
        allArrayList = getIntent().getParcelableArrayListExtra(TAB_ALL);
        favArrayList = getIntent().getParcelableArrayListExtra(TAB_FAVOURITE);

        if (allPosition != -1) {
            if (tabIndex == 0) {
                Task task = allArrayList.get(allPosition);
                fieldTitle.setText(task.getTitle());
                fieldDescription.setText(task.getDescription());
            }
            else if (tabIndex == 1) {
                Task task = favArrayList.get(favPosition);
                fieldTitle.setText(task.getTitle());
                fieldDescription.setText(task.getDescription());
            }
        }
    }

    @InjectOnClickListener(R.id.save_button)
    private void onSaveButtonClick(View view) {
        Intent intent = new Intent(NewTaskActivity.this, TaskActivity.class);
        String title = fieldTitle.getText().toString();
        String description = fieldDescription.getText().toString();

        if (allPosition != -1) {
            if (tabIndex == 0) {
                if (favPosition != -1) {
                    favArrayList.get(favPosition).setTitle(title);
                    favArrayList.get(favPosition).setDescription(description);
                }
                allArrayList.get(allPosition).setTitle(title);
                allArrayList.get(allPosition).setDescription(description);
            }
            else if (tabIndex == 1) {
                allArrayList.get(allPosition).setTitle(title);
                allArrayList.get(allPosition).setDescription(description);
                favArrayList.get(favPosition).setTitle(title);
                favArrayList.get(favPosition).setDescription(description);
            }
        }
        else {
            Task task = new Task(fieldTitle.getText().toString(), fieldDescription.getText().toString());
            allArrayList.add(task);
            if (tabIndex == 1) favArrayList.add(task);
        }
        intent.putParcelableArrayListExtra(TAB_FAVOURITE, favArrayList);
        intent.putParcelableArrayListExtra(TAB_ALL, allArrayList);
        intent.putExtra(STORAGE, storage);
        intent.putExtra(EDITABLE, true);
        startActivity(intent);
    }

}
